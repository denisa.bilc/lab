package benchmarks;

import domain.Order;
import repositories.InMemoryRepository;
import repositories.objects.*;
import repositories.primitives.IntObjMapKolobokeRepository;
import repositories.primitives.IntObjMapTroveRepository;
import repositories.primitives.IntObjectMapFastutilRepository;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Measurement(time = 5)
@Warmup(time = 5)
public class ContainsBenchmark {
    @State(Scope.Benchmark)
    public static class InitialState {
        @Param({"1000", "10000"})
        int size;
        List<Order> randomOrders = new ArrayList<>();

        @Setup(Level.Trial)
        public void doSetup() {
            randomOrders = new ArrayList<>();
            Random random = new Random();

            for (int i = 0; i < size; i++) {
                int price = random.nextInt();
                int quantity = random.nextInt();

                randomOrders.add(new Order(i, price, quantity));
            }

            Collections.shuffle(randomOrders);
        }
    }

    @State(Scope.Benchmark)
    public static class RepositoryState {
        InMemoryRepository<Order> repository;
        @Param({"ArrayListJdk", "ConcurrentHashMap", "HashSetJdk",
                "HashSetKoloboke", "EclipseList", "TreeSetJdk",
                "IntOnjMapFastutil", "IntObjMapKoloboke", "IntObjMapTrove"})
        RepositoryTypeEnum repositoryType;

        @Setup(Level.Trial)
        public void doSetup(InitialState state) {
            switch (repositoryType.toString()) {
                case "ConcurrentHashMap":
                    repository = new ConcurrentHashMapRepository<>();
                    break;
                case "HashSetJdk":
                    repository = new HashSetBasedRepository<>();
                    break;
                case "HashSetKoloboke":
                    repository = new HashSetKolobokeRepository<>();
                    break;
                case "EclipseList":
                    repository = new ListEclipseRepository<>();
                    break;
                case "TreeSetJdk":
                    repository = new TreeSetBasedRepository<>();
                    break;
                case "IntOnjMapFastutil":
                    repository = new IntObjectMapFastutilRepository<>();
                    break;
                case "IntObjMapKoloboke":
                    repository = new IntObjMapKolobokeRepository<>();
                    break;
                case "IntObjMapTrove":
                    repository = new IntObjMapTroveRepository<>();
                    break;
                default:
                    repository = new ArrayListBasedRepository<>();
                    break;
            }

            for (Order order : state.randomOrders) {
                repository.add(order);
            }
        }
    }

    @Benchmark
    public void contains(RepositoryState repoState, InitialState state, Blackhole blackhole) {
        for (Order order : state.randomOrders)
            blackhole.consume(repoState.repository.contains(order));
    }
}
