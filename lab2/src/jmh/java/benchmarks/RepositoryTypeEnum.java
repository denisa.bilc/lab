package benchmarks;

public enum RepositoryTypeEnum {
    ArrayListJdk, ConcurrentHashMap, HashSetJdk, HashSetKoloboke, EclipseList,
    TreeSetJdk, IntOnjMapFastutil, IntObjMapKoloboke, IntObjMapTrove
}
