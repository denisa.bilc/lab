package domain;

public interface Entity {
    int getId();

    void setId(int id);
}
