package repositories;

import domain.Entity;

public interface InMemoryRepository<T extends Entity> {
    void add(T element);

    boolean contains(T element);

    void remove(T element);
}
