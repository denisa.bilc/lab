package repositories.primitives;

import domain.Entity;
import repositories.InMemoryRepository;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

public class IntObjectMapFastutilRepository<V extends Entity> implements InMemoryRepository<V> {
    private final Int2ObjectMap<V> int2ObjectOpenHashMap;

    public IntObjectMapFastutilRepository() {
        this.int2ObjectOpenHashMap = new Int2ObjectOpenHashMap<>();
    }

    @Override
    public void add(V element) {
        int2ObjectOpenHashMap.put(element.getId(), element);
    }

    @Override
    public boolean contains(V element) {
        return int2ObjectOpenHashMap.containsKey(element.getId())
                && int2ObjectOpenHashMap.get(element.getId()).equals(element);
    }

    @Override
    public void remove(V element) {
        int2ObjectOpenHashMap.remove(element.getId());
    }
}
