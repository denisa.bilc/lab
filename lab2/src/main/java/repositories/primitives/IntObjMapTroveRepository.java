package repositories.primitives;

import domain.Entity;
import repositories.InMemoryRepository;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

public class IntObjMapTroveRepository<V extends Entity> implements InMemoryRepository<V> {
    private final TIntObjectMap<V> tIntObjectMap;

    public IntObjMapTroveRepository(){
        tIntObjectMap =  new TIntObjectHashMap<>();
    }
    @Override
    public void add(V element) {
        tIntObjectMap.put(element.getId(), element);
    }

    @Override
    public boolean contains(V element) {
        return tIntObjectMap.containsKey(element.getId())
                && tIntObjectMap.get(element.getId()).equals(element);
    }

    @Override
    public void remove(V element) {
        tIntObjectMap.remove(element.getId());
    }
}
