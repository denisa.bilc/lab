package repositories.primitives;

import domain.Entity;
import repositories.InMemoryRepository;
import com.koloboke.collect.map.hash.HashIntObjMap;
import com.koloboke.collect.map.hash.HashIntObjMaps;

public class IntObjMapKolobokeRepository<T extends Entity> implements InMemoryRepository<T> {
    private final HashIntObjMap<T> hashIntObjMap;

    public IntObjMapKolobokeRepository() {
        this.hashIntObjMap = HashIntObjMaps.newMutableMap();
    }

    @Override
    public void add(T element) {
        hashIntObjMap.put(element.getId(), element);
    }

    @Override
    public boolean contains(T element) {
        return hashIntObjMap.containsKey(element.getId())
                && hashIntObjMap.get(element.getId()).equals(element);
    }

    @Override
    public void remove(T element) {
        hashIntObjMap.remove(element.getId());
    }
}
