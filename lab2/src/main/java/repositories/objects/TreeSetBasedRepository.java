package repositories.objects;

import domain.Entity;
import repositories.InMemoryRepository;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T extends Entity> implements InMemoryRepository<T> {
    private final Set<T> treeSet;

    public TreeSetBasedRepository() {
        this.treeSet = new TreeSet<>();
    }


    @Override
    public void add(T element) {
        treeSet.add(element);
    }

    @Override
    public boolean contains(T element) {
        return treeSet.contains(element);
    }

    @Override
    public void remove(T element) {
        treeSet.remove(element);
    }
}
