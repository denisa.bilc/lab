package repositories.objects;

import domain.Entity;
import repositories.InMemoryRepository;
import com.koloboke.collect.set.ObjSet;
import com.koloboke.collect.set.hash.HashObjSets;

public class HashSetKolobokeRepository<T extends Entity> implements InMemoryRepository<T> {
    private final ObjSet<T> objSet;

    public HashSetKolobokeRepository() {
        this.objSet = HashObjSets.newMutableSet();
    }


    public void add(T element) {
        objSet.add(element);
    }

    @Override
    public boolean contains(T element) {
        return objSet.contains(element);
    }

    @Override
    public void remove(T element) {
        objSet.remove(element);
    }
}
