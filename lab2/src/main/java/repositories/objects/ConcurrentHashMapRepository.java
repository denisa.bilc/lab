package repositories.objects;

import domain.Entity;
import repositories.InMemoryRepository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapRepository<V extends Entity> implements InMemoryRepository<V> {
    private final Map<Integer, V> concurrentHashMap;

    public ConcurrentHashMapRepository() {
        this.concurrentHashMap = new ConcurrentHashMap<>();
    }

    @Override
    public void add(V element) {
        concurrentHashMap.put(element.getId(), element);
    }

    @Override
    public boolean contains(V element) {
        return concurrentHashMap.containsKey(element.getId())
                && concurrentHashMap.get(element.getId()).equals(element);
    }

    @Override
    public void remove(V element) {
        concurrentHashMap.remove(element.getId());
    }
}
