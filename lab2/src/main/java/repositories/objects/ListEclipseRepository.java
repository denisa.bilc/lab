package repositories.objects;

import domain.Entity;
import repositories.InMemoryRepository;
import org.eclipse.collections.api.factory.Lists;

import java.util.List;

public class ListEclipseRepository<T extends Entity> implements InMemoryRepository<T> {
    private final List<T> mutableList;

    public ListEclipseRepository() {
        this.mutableList = Lists.mutable.empty();
    }

    @Override
    public void add(T element) {
        mutableList.add(element);
    }

    @Override
    public boolean contains(T element) {
        return mutableList.contains(element);
    }

    @Override
    public void remove(T element) {
        mutableList.remove(element);
    }
}
