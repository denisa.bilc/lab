package com.example;

import calculator.OperationFactory;
import calculator.operations.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestOperationFactory {
    private OperationFactory operationFactory;

    @BeforeEach
    public void setup() {
        this.operationFactory = new OperationFactory();
    }

    @Test
    void shouldReturnAddition() {
        Operation operation = operationFactory.getOperation("+");

        assertTrue(operation instanceof Addition);
    }

    @Test
    void shouldReturnSubtraction() {
        Operation operation = operationFactory.getOperation("-");

        assertTrue(operation instanceof Subtraction);
    }

    @Test
    void shouldReturnDivision() {
        Operation operation = operationFactory.getOperation("/");

        assertTrue(operation instanceof Division);
    }

    @Test
    void shouldReturnMultiplication() {
        Operation operation = operationFactory.getOperation("*");

        assertTrue(operation instanceof Multiplication);
    }

    @Test
    void shouldReturnMax() {
        Operation operation = operationFactory.getOperation("max");

        assertTrue(operation instanceof Max);
    }

    @Test
    void shouldReturnMin() {
        Operation operation = operationFactory.getOperation("min");

        assertTrue(operation instanceof Min);
    }

    @Test
    void shouldReturnSqrt() {
        Operation operation = operationFactory.getOperation("sqrt");

        assertTrue(operation instanceof Sqrt);
    }

    @Test
    void shouldReturnNull() {
        Operation operation = operationFactory.getOperation("aaaa");

        assertThat(operation, is(equalTo(null)));
    }
}
