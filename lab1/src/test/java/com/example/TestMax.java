package com.example;

import calculator.operations.Max;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class TestMax {
    private Max max;

    @BeforeEach
    public void setup() {
        max = new Max();
    }

    @Test
    public void correctResultForPositiveNumbers() {
        double operand1 = 3.0;
        double operand2 = 5.0;

        assertThat(max.calculate(operand1, operand2), is(equalTo(5.0)));
    }

    @Test
    public void correctResultForNegativeNumbers() {
        double operand1 = -3.0;
        double operand2 = -5.0;

        assertThat(max.calculate(operand1, operand2), is(equalTo(-3.0)));
    }

    @Test
    public void correctResultForOneNegativeNumber() {
        double operand1 = -3.0;
        double operand2 = 5.0;

        assertThat(max.calculate(operand1, operand2), is(equalTo(5.0)));
    }
}
