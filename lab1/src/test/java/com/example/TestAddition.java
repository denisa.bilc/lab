package com.example;

import calculator.operations.Addition;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import static org.hamcrest.MatcherAssert.assertThat;

public class TestAddition {
    private Addition addition;

    @BeforeEach
    public void setup() {
        addition = new Addition();
    }

    @Test
    public void correctResultForPositiveNumbers() {
        double operand1 = 3.0;
        double operand2 = 5.0;

        assertThat(addition.calculate(operand1, operand2), is(equalTo(8.0)));
    }

    @Test
    public void correctResultForNegativeNumbers() {
        double operand1 = -3.0;
        double operand2 = -5.0;

        assertThat(addition.calculate(operand1, operand2), is(equalTo(-8.0)));
    }

    @Test
    public void correctResultForOneNegativeNumber() {
        double operand1 = -3.0;
        double operand2 = 5.0;

        assertThat(addition.calculate(operand1, operand2), is(equalTo(2.0)));
    }

}
