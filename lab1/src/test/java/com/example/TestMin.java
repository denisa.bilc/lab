package com.example;

import calculator.operations.Min;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class TestMin {
    private Min min;

    @BeforeEach
    public void setup() {
        min = new Min();
    }

    @Test
    public void correctResultForPositiveNumbers() {
        double operand1 = 3.0;
        double operand2 = 5.0;

        assertThat(min.calculate(operand1, operand2), is(equalTo(3.0)));
    }

    @Test
    public void correctResultForNegativeNumbers() {
        double operand1 = -3.0;
        double operand2 = -5.0;

        assertThat(min.calculate(operand1, operand2), is(equalTo(-5.0)));
    }

    @Test
    public void correctResultForOneNegativeNumber() {
        double operand1 = -3.0;
        double operand2 = 5.0;

        assertThat(min.calculate(operand1, operand2), is(equalTo(-3.0)));
    }

}
