package com.example;

import calculator.operations.Multiplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class TestMultiplication {
    private Multiplication multiplication;

    @BeforeEach
    public void setup() {
        multiplication = new Multiplication();
    }

    @Test
    public void correctResultForPositiveNumbers() {
        double operand1 = 5.0;
        double operand2 = 2.0;

        assertThat(multiplication.calculate(operand1, operand2), is(equalTo(10.0)));
    }

    @Test
    public void correctResultForNegativeNumbers() {
        double operand1 = -5.0;
        double operand2 = -2.0;

        assertThat(multiplication.calculate(operand1, operand2), is(equalTo(10.0)));
    }

    @Test
    public void correctResultForOneNegativeNumber() {
        double operand1 = -5.0;
        double operand2 = 2.0;

        assertThat(multiplication.calculate(operand1, operand2), is(equalTo(-10.0)));
    }
}
