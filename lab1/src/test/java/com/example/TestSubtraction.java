package com.example;

import calculator.operations.Subtraction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;


public class TestSubtraction {
    private Subtraction subtraction;

    @BeforeEach
    public void setup() {
        subtraction = new Subtraction();
    }

    @Test
    public void correctResultForPositiveNumbers() {
        double operand1 = 5;
        double operand2 = 9;

        assertThat(subtraction.calculate(operand1, operand2), is(equalTo(-4.0)));
        assertThat(subtraction.calculate(operand2, operand1), is(equalTo(4.0)));
    }

    @Test
    public void correctResultForNegativeNumbers() {
        double operand1 = -5;
        double operand2 = -9;

        assertThat(subtraction.calculate(operand1, operand2), is(equalTo(4.0)));
    }

    @Test
    public void correctResultForOneNegativeNumber() {
        double operand1 = -5;
        double operand2 = 9;

        assertThat(subtraction.calculate(operand1, operand2), is(equalTo(-14.0)));
        assertThat(subtraction.calculate(operand2, operand1), is(equalTo(14.0)));
    }
}
