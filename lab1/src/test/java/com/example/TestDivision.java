package com.example;

import calculator.exceptions.OperationException;
import calculator.operations.Division;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestDivision {
    private Division division;

    @BeforeEach
    public void setup() {
        division = new Division();
    }

    @Test
    public void correctResultForPositiveNumbers() {
        double operand1 = 5.0;
        double operand2 = 2.0;

        assertThat(division.calculate(operand1, operand2), is(equalTo(2.5)));
    }

    @Test
    public void correctResultForNegativeNumbers() {
        double operand1 = -5.0;
        double operand2 = -2.0;

        assertThat(division.calculate(operand1, operand2), is(equalTo(2.5)));
    }

    @Test
    public void correctResultForOneNegativeNumber() {
        double operand1 = -5.0;
        double operand2 = 2.0;

        assertThat(division.calculate(operand1, operand2), is(equalTo(-2.5)));
    }

    @Test
    public void correctResultForDivisionByZero() {
        double operand1 = 5.0;
        double operand2 = 0;

        assertThrows(OperationException.class, () -> division.calculate(operand1, operand2));
    }
}
