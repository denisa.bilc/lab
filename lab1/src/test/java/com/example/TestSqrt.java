package com.example;

import calculator.exceptions.OperationException;
import calculator.operations.Sqrt;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestSqrt {
    private Sqrt sqrt;

    @BeforeEach
    public void setup() {
        sqrt = new Sqrt();
    }

    @Test
    public void correctResultForPositiveNumbers() {
        double operand = 25;

        assertThat(sqrt.calculate(operand), is(equalTo(5.0)));
    }

    @Test
    public void correctResultForNegativeNumber() {
        double operand = -3.0;

        assertThrows(OperationException.class, () -> sqrt.calculate(operand));
    }

    @Test
    public void correctResultForZero() {
        double operand = 0.0;

        assertThrows(OperationException.class, () -> sqrt.calculate(operand));
    }
}
