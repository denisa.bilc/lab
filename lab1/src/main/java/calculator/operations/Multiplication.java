package calculator.operations;

public class Multiplication implements MultiplicationOperation {
    @Override
    public double calculate(double... operands) {
        return operands[0] * operands[1];
    }
}
