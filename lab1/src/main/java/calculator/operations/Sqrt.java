package calculator.operations;

import calculator.exceptions.OperationException;

import static java.lang.Math.sqrt;

public class Sqrt implements SqrtOperation {
    @Override
    public double calculate(double... operands) {
        if (operands[0] <= 0) {
            throw new OperationException("Input number for sqrt must be greater than 0!");
        }

        return sqrt(operands[0]);
    }
}
