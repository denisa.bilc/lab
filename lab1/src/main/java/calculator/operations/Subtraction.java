package calculator.operations;

public class Subtraction implements SubtractionOperation {
    @Override
    public double calculate(double... operands) {
        return operands[0] - operands[1];
    }
}
