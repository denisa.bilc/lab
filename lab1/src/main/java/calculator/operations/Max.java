package calculator.operations;

public class Max implements MaxOperation {
    @Override
    public double calculate(double... operands) {
        if (operands[0] > operands[1])
            return operands[0];

        return operands[1];
    }
}
