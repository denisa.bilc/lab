package calculator.operations;

public class Addition implements AdditionOperation {
    @Override
    public double calculate(double... operands) {
        return operands[0] + operands[1];
    }
}
