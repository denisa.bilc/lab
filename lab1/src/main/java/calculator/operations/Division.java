package calculator.operations;

import calculator.exceptions.OperationException;

public class Division implements DivisionOperation {
    @Override
    public double calculate(double... operands) {
        if (operands[1] == 0) {
            throw new OperationException("The divisor must different form 0!");
        }

        return operands[0] / operands[1];
    }
}
