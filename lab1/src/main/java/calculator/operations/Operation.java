package calculator.operations;

public interface Operation {
    double calculate(double... operands);
}
