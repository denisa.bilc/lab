package calculator;

import calculator.operations.*;

import java.util.HashMap;
import java.util.Map;

public class OperationFactory {
    private final Map<String, Operation> operations = new HashMap<>();

    public OperationFactory() {
        operations.put("+", new Addition());
        operations.put("-", new Subtraction());
        operations.put("*", new Multiplication());
        operations.put("/", new Division());
        operations.put("min", new Min());
        operations.put("max", new Max());
        operations.put("sqrt", new Sqrt());
    }

    public Operation getOperation(String operator) {
        return operations.get(operator);
    }
}
