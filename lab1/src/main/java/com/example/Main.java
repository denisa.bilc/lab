package com.example;


import calculator.*;
import calculator.exceptions.OperationException;
import calculator.operations.Operation;
import calculator.operations.UnaryOperation;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double result = 0;
        OperationFactory operationFactory = new OperationFactory();

        System.out.println("Current result: " + result);

        while (true) {

            try {
                System.out.println("Enter the operation (+, -, *, /, min, max, sqrt) or 'q' to quit: ");
                String operationReceived = scanner.nextLine();

                if (operationReceived.equals("q")) {
                    break;
                }

                Operation operation = operationFactory.getOperation(operationReceived);

                if (operation != null) {
                    if (operation instanceof UnaryOperation) {
                        result = operation.calculate(result);
                    } else {
                        System.out.println("Enter a number: ");
                        double numberReceived = Double.parseDouble(scanner.nextLine());
                        result = operation.calculate(result, numberReceived);
                    }
                } else {
                    System.out.println("Invalid operation " + operationReceived + "!");
                }

                System.out.println("Result: " + result);

            } catch (NumberFormatException numberFormatException) {
                System.out.println("Wrong number format!");
            } catch (OperationException operationException) {
                System.out.println(operationException.getMessage());
            }
        }
    }
}