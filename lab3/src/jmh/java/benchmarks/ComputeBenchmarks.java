package benchmarks;

import org.app.computers.ComputeBigDecimal;
import org.app.computers.ComputeDoublePrimitive;
import org.app.computers.ComputeDoubleWrapper;
import org.app.generators.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.math.BigDecimal;

@Measurement(time = 5)
@Warmup(time = 10)
public class ComputeBenchmarks {
    @State(Scope.Benchmark)
    public static class GeneratorState {
        @Param({"BIG_DECIMALS", "DOUBLES_WRAPPER", "DOUBLES_PRIMITIVE"})
        ElementsType elementsType;
        @Param({"RANDOM", "SORTED_ASC", "SORTED_DESC"})
        GenerationType generationType;
        GeneratorObjects<BigDecimal> generatorBigDecimal;
        GeneratorObjects<Double> generatorDouble;
        GeneratorPrimitives generatorPrimitives;

        @Setup(Level.Trial)
        public void setup() {
            switch (elementsType) {
                case BIG_DECIMALS:
                    generatorBigDecimal = new BigDecimalGenerator(100000);
                    callGenerationStrategy(generatorBigDecimal);
                    break;
                case DOUBLES_PRIMITIVE:
                    generatorPrimitives = new DoublePrimitiveGenerator(100000);
                    callGenerationStrategy(generatorPrimitives);
                    break;
                case DOUBLES_WRAPPER:
                    generatorDouble = new DoubleWrapperGenerator(100000);
                    callGenerationStrategy(generatorDouble);
                    break;
            }
        }

        public void callGenerationStrategy(Generator generator) {
            switch (generationType) {
                case RANDOM:
                    generator.generateRandom();
                    break;
                case SORTED_ASC:
                    generator.generateAscending();
                    break;
                case SORTED_DESC:
                    generator.generateDescending();
                    break;
            }
        }
    }

    @State(Scope.Benchmark)
    public static class ComputeState {
        interface MyFunc {
            Object exec();
        }

        MyFunc execSum;
        MyFunc execAvg;
        MyFunc execPrintFirst10;

        @Setup(Level.Trial)
        public void setup(GeneratorState generatorState) {
            switch (generatorState.elementsType) {
                case BIG_DECIMALS:
                    ComputeBigDecimal bigDecimalComputeObjects = new ComputeBigDecimal(
                            generatorState.generatorBigDecimal.getGeneratedElements()
                    );
                    execSum = bigDecimalComputeObjects::sum;
                    execAvg = bigDecimalComputeObjects::average;
                    execPrintFirst10 = bigDecimalComputeObjects::firstTenPercent;
                    break;
                case DOUBLES_WRAPPER:
                    ComputeDoubleWrapper doubleComputeObjects = new ComputeDoubleWrapper(
                            generatorState.generatorDouble.getGeneratedElements()
                    );
                    execSum = doubleComputeObjects::sum;
                    execAvg = doubleComputeObjects::average;
                    execPrintFirst10 = doubleComputeObjects::firstTenPercent;
                    break;
                case DOUBLES_PRIMITIVE:
                    ComputeDoublePrimitive computeDoublePrimitive = new ComputeDoublePrimitive(
                            generatorState.generatorPrimitives.getGeneratedElements()
                    );
                    execSum = computeDoublePrimitive::sum;
                    execAvg = computeDoublePrimitive::average;
                    execPrintFirst10 = computeDoublePrimitive::firstTenPercent;
                    break;
            }
        }
    }

    @Benchmark
    public void sum(ComputeState computeState, Blackhole blackhole) {
        blackhole.consume(computeState.execSum.exec());
    }

    @Benchmark
    public void average(ComputeState computeState, Blackhole blackhole) {
        blackhole.consume(computeState.execAvg.exec());
    }

    @Benchmark
    public void firstTenPercent(ComputeState computeState, Blackhole blackhole) {
        blackhole.consume(computeState.execPrintFirst10.exec());
    }
}
