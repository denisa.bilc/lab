package benchmarks;

public enum ElementsType {
    BIG_DECIMALS, DOUBLES_WRAPPER, DOUBLES_PRIMITIVE
}