package benchmarks;

public enum GenerationType {
    RANDOM, SORTED_ASC, SORTED_DESC
}
