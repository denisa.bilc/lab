package org.app.computers;

import org.app.exceptions.OperationException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ComputeBigDecimal {
    private List<BigDecimal> bigDecimalList;

    public ComputeBigDecimal(List<BigDecimal> bigDecimals) {
        bigDecimalList = new ArrayList<>(bigDecimals);
    }

    public BigDecimal sum() {
        return bigDecimalList.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal average() {
        if (bigDecimalList.size() == 0) {
            throw new OperationException("List is empty! Cannot divide by zero!");
        }

        return bigDecimalList.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(BigDecimal.valueOf(bigDecimalList.size()));
    }

    public List<BigDecimal> firstTenPercent() {
        int percentage = (int) Math.ceil(bigDecimalList.size() * 0.1);

        return bigDecimalList.stream()
                .sorted(Comparator.reverseOrder())
                .limit(percentage)
                .collect(Collectors.toList());
    }

    public void setBigDecimalList(List<BigDecimal> bigDecimals) {
        this.bigDecimalList = bigDecimals;
    }
}
