package org.app.computers;

import org.app.exceptions.OperationException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ComputeDoubleWrapper {
    private List<Double> doubleList;

    public ComputeDoubleWrapper(List<Double> doubles) {
        doubleList = new ArrayList<>(doubles);
    }

    public Double sum() {
        return doubleList.stream()
                .reduce(0.0, Double::sum);
    }

    public Double average() {
        if (doubleList.size() == 0) {
            throw new OperationException("List is empty! Cannot divide by zero!");
        }

        return doubleList.stream()
                .mapToDouble(d -> d)
                .average()
                .orElse(0.0);
    }

    public List<Double> firstTenPercent() {
        int percentage = (int) Math.ceil(doubleList.size() * 0.1);

        return doubleList.stream()
                .sorted(Comparator.reverseOrder())
                .limit(percentage)
                .collect(Collectors.toList());
    }

    public void setDoubleList(List<Double> doubles) {
        this.doubleList = doubles;
    }
}
