package org.app.computers;

import org.app.exceptions.OperationException;

import java.util.Arrays;
import java.util.Comparator;

public class ComputeDoublePrimitive {
    private double[] doubleArray;

    public ComputeDoublePrimitive(double[] doubles) {
        doubleArray = doubles;
    }

    public double sum() {
        return Arrays.stream(doubleArray)
                .sum();
    }

    public double average() {
        if (doubleArray.length == 0) {
            throw new OperationException("List is empty! Cannot divide by zero!");
        }

        return Arrays.stream(doubleArray)
                .average()
                .orElse(0);
    }

    public double[] firstTenPercent() {
        int percentage = (int) Math.ceil(doubleArray.length * 0.1);

        return Arrays.stream(doubleArray)
                .boxed()
                .sorted(Comparator.reverseOrder())
                .limit(percentage)
                .mapToDouble(Double::doubleValue)
                .toArray();

    }

    public void setDoubleArray(double[] doubleArray) {
        this.doubleArray = doubleArray;
    }
}
