package org.app.exceptions;

public class OperationException extends RuntimeException {
    public OperationException(String errorMessage) {
        super(errorMessage);
    }
}
