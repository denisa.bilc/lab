package org.app.generators;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class BigDecimalGenerator implements GeneratorObjects<BigDecimal> {

    private final List<BigDecimal> bigIntegerList;
    private final int size;

    public BigDecimalGenerator(int size) {
        this.bigIntegerList = new ArrayList<>(size);
        this.size = size;
    }

    @Override
    public void generateRandom() {
        Random random = new Random();
        Supplier<BigDecimal> supplier = () ->
                BigDecimal.valueOf(random.nextDouble() * 100).setScale(2, RoundingMode.HALF_UP);

        for (int i = 0; i < size; i++) {
            BigDecimal element = supplier.get();
            bigIntegerList.add(element);
        }
    }

    @Override
    public void generateAscending() {
        BigDecimal currentElement = BigDecimal.ZERO;

        for (int i = 0; i < size; i++) {
            bigIntegerList.add(currentElement);
            currentElement = currentElement.add(BigDecimal.valueOf(0.5));
        }
    }

    @Override
    public void generateDescending() {
        generateAscending();
        Collections.reverse(bigIntegerList);
    }

    @Override
    public List<BigDecimal> getGeneratedElements() {
        return bigIntegerList;
    }
}
