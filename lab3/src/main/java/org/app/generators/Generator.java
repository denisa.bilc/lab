package org.app.generators;

public interface Generator {
    void generateRandom();

    void generateAscending();

    void generateDescending();
}
