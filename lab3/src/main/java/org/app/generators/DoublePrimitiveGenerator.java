package org.app.generators;

import java.util.*;
import java.util.stream.Collectors;

public class DoublePrimitiveGenerator implements GeneratorPrimitives {

    private final double[] doublesArray;
    private final int size;

    public DoublePrimitiveGenerator(int size) {

        this.doublesArray = new double[size];
        this.size = size;
    }

    @Override
    public void generateRandom() {
        Random random = new Random();

        for (int i = 0; i < size; i++) {
            doublesArray[i] = random.nextDouble();
        }
    }

    @Override
    public void generateAscending() {
        double currentValue = 0.0;

        for (int i = 0; i < size; i++) {
            doublesArray[i] = currentValue;
            currentValue += 0.5;
        }
    }

    @Override
    public void generateDescending() {
        generateAscending();
        List<Double> doublesList = Arrays.stream(doublesArray)
                .boxed()
                .collect(Collectors.toList());
        Collections.reverse(doublesList);

        double[] reversedArray = doublesList.stream().
                mapToDouble(Double::doubleValue)
                .toArray();
        System.arraycopy(reversedArray, 0, doublesArray, 0, doublesArray.length);
    }

    @Override
    public double[] getGeneratedElements() {
        return doublesArray;
    }
}
