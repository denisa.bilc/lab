package org.app.generators;

public interface GeneratorPrimitives extends Generator {
    double[] getGeneratedElements();
}
