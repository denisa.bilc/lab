package org.app.generators;

import java.util.List;

public interface GeneratorObjects<T> extends Generator {
    List<T> getGeneratedElements();
}
