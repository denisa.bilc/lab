package org.app.generators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class DoubleWrapperGenerator implements GeneratorObjects<Double> {
    private final List<Double> doubleList;
    private final int size;

    public DoubleWrapperGenerator(int size) {
        this.doubleList = new ArrayList<>(size);
        this.size = size;
    }

    @Override
    public void generateRandom() {
        Random random = new Random();
        Supplier<Double> supplier = random::nextDouble;

        for (int i = 0; i < size; i++) {
            Double randomDouble = supplier.get();
            doubleList.add(randomDouble);
        }
    }

    @Override
    public void generateAscending() {
        Double currentElement = 0.0;

        for (int i = 0; i < size; i++) {
            doubleList.add(currentElement);
            currentElement += 0.5;
        }
    }

    @Override
    public void generateDescending() {
        generateAscending();
        Collections.reverse(doubleList);
    }

    @Override
    public List<Double> getGeneratedElements() {
        return doubleList;
    }
}
