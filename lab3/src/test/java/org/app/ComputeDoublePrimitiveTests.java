package org.app;

import org.app.computers.ComputeDoublePrimitive;
import org.app.exceptions.OperationException;
import org.app.generators.DoublePrimitiveGenerator;
import org.app.generators.GeneratorPrimitives;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ComputeDoublePrimitiveTests {
    private ComputeDoublePrimitive computeDoublePrimitive;
    private double[] emptyArray;
    private double[] arrayWithNegativeValues;

    @BeforeEach
    public void setup() {
        emptyArray = new double[]{};
        arrayWithNegativeValues = new double[]{-2, 10, 2, 0};

        GeneratorPrimitives generatorPrimitives = new DoublePrimitiveGenerator(20);
        generatorPrimitives.generateDescending();
        computeDoublePrimitive = new ComputeDoublePrimitive(
                generatorPrimitives.getGeneratedElements()
        );
    }

    @Test
    public void testSum() {
        double sum = computeDoublePrimitive.sum();

        assertThat(sum, is(equalTo(95.0)));
    }

    @Test
    public void testAverage() {
        double average = computeDoublePrimitive.average();

        assertThat(average, is(equalTo(4.75)));
    }

    @Test
    public void testFirstTenPercent() {
        double[] firstTenPercent = computeDoublePrimitive.firstTenPercent();
        double[] expectedResult = new double[]{9.5, 9};

        assertThat(firstTenPercent, is(equalTo(expectedResult)));
    }

    @Test
    public void testSumEmptyList() {
        computeDoublePrimitive.setDoubleArray(emptyArray);
        double sum = computeDoublePrimitive.sum();

        assertThat(sum, is(equalTo(0.0)));
    }

    @Test
    public void testAverageEmptyList() {
        computeDoublePrimitive.setDoubleArray(emptyArray);

        assertThrows(OperationException.class, () -> computeDoublePrimitive.average());
    }

    @Test
    public void testFirstTenPercentEmptyList() {
        computeDoublePrimitive.setDoubleArray(emptyArray);
        double[] firstTenPercent = computeDoublePrimitive.firstTenPercent();

        assertThat(firstTenPercent.length, is(equalTo(0)));
    }

    @Test
    public void testSumListWithNegativeValues() {
        computeDoublePrimitive.setDoubleArray(arrayWithNegativeValues);
        double sum = computeDoublePrimitive.sum();

        assertThat(sum, is(equalTo(10.0)));
    }

    @Test
    public void testAverageListWithNegativeValues() {
        computeDoublePrimitive.setDoubleArray(arrayWithNegativeValues);
        double average = computeDoublePrimitive.average();

        assertThat(average, is(equalTo(2.5)));
    }

    @Test
    public void testFirstTenPercentListWithNegativeValues() {
        double[] expectedResult = new double[]{10};
        computeDoublePrimitive.setDoubleArray(arrayWithNegativeValues);
        double[] firstTenPercent = computeDoublePrimitive.firstTenPercent();

        assertThat(firstTenPercent, is(equalTo(expectedResult)));
    }

}
