package org.app;

import org.app.computers.ComputeBigDecimal;
import org.app.exceptions.OperationException;
import org.app.generators.BigDecimalGenerator;
import org.app.generators.GeneratorObjects;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ComputeBigDecimalTests {
    private ComputeBigDecimal computeBigDecimal;
    private List<BigDecimal> emptyList;
    private List<BigDecimal> listWithNegativeValues;

    @BeforeEach
    public void setup() {
        emptyList = new ArrayList<>();
        listWithNegativeValues = Arrays.asList(new BigDecimal("-2"),
                new BigDecimal("10"),
                new BigDecimal("2"),
                new BigDecimal("0")
        );

        GeneratorObjects<BigDecimal> generator = new BigDecimalGenerator(20);
        generator.generateDescending();
        computeBigDecimal = new ComputeBigDecimal(generator.getGeneratedElements());
    }

    @Test
    public void testSum() {
        BigDecimal sum = computeBigDecimal.sum();

        assertThat(sum, is(equalTo(new BigDecimal("95.0"))));
    }

    @Test
    public void testAverage() {
        BigDecimal average = computeBigDecimal.average();

        assertThat(average, is(equalTo(new BigDecimal("4.75"))));
    }

    @Test
    public void testFirstTenPercent() {
        List<BigDecimal> firstTenPercent = computeBigDecimal.firstTenPercent();
        List<BigDecimal> expectedResult = Arrays.asList(new BigDecimal("9.5"), new BigDecimal("9.0"));


        assertThat(firstTenPercent, is(equalTo(expectedResult)));
    }

    @Test
    public void testSumEmptyList() {
        computeBigDecimal.setBigDecimalList(emptyList);
        BigDecimal sum = computeBigDecimal.sum();

        assertThat(sum, is(equalTo(new BigDecimal("0"))));
    }

    @Test
    public void testAverageEmptyList() {
        computeBigDecimal.setBigDecimalList(emptyList);

        assertThrows(OperationException.class, () -> computeBigDecimal.average());
    }

    @Test
    public void testFirstTenPercentEmptyList() {
        computeBigDecimal.setBigDecimalList(emptyList);
        List<BigDecimal> firstTenPercent = computeBigDecimal.firstTenPercent();

        assertThat(firstTenPercent.size(), is(equalTo(0)));
    }

    @Test
    public void testSumListWithNegativeValues() {
        computeBigDecimal.setBigDecimalList(listWithNegativeValues);
        BigDecimal sum = computeBigDecimal.sum();

        assertThat(sum, is(equalTo(new BigDecimal("10"))));
    }

    @Test
    public void testAverageListWithNegativeValues() {
        computeBigDecimal.setBigDecimalList(listWithNegativeValues);
        BigDecimal average = computeBigDecimal.average();

        assertThat(average, is(equalTo(new BigDecimal("2.5"))));
    }

    @Test
    public void testFirstTenPercentListWithNegativeValues() {
        List<BigDecimal> expectedResult = List.of(new BigDecimal("10"));
        computeBigDecimal.setBigDecimalList(listWithNegativeValues);
        List<BigDecimal> firstTenPercent = computeBigDecimal.firstTenPercent();

        assertThat(firstTenPercent, is(equalTo(expectedResult)));
    }
}
