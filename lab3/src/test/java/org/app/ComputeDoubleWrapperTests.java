package org.app;

import org.app.computers.ComputeDoubleWrapper;
import org.app.exceptions.OperationException;
import org.app.generators.DoubleWrapperGenerator;
import org.app.generators.GeneratorObjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ComputeDoubleWrapperTests {
    private ComputeDoubleWrapper computeDoubleWrapper;
    private List<Double> emptyList;
    private List<Double> listWithNegativeValues;

    @BeforeEach
    public void setup() {
        emptyList = new ArrayList<>();
        listWithNegativeValues = Arrays.asList(new Double("-2"),
                new Double("10"),
                new Double("2"),
                new Double("0")
        );

        GeneratorObjects<Double> generator = new DoubleWrapperGenerator(20);
        generator.generateDescending();
        computeDoubleWrapper = new ComputeDoubleWrapper(
                generator.getGeneratedElements()
        );
    }

    @Test
    public void testSum() {
        Double sum = computeDoubleWrapper.sum();

        assertThat(sum, is(equalTo(95.0)));
    }

    @Test
    public void testAverage() {
        Double average = computeDoubleWrapper.average();

        assertThat(average, is(equalTo(4.75)));
    }

    @Test
    public void testFirstTenPercent() {
        List<Double> firstTenPercent = computeDoubleWrapper.firstTenPercent();
        List<Double> expectedResult = Arrays.asList(9.5, 9.0);

        assertThat(firstTenPercent, is(equalTo(expectedResult)));
    }

    @Test
    public void testSumEmptyList() {
        computeDoubleWrapper.setDoubleList(emptyList);
        Double sum = computeDoubleWrapper.sum();

        assertThat(sum, is(equalTo(0.0)));
    }

    @Test
    public void testAverageEmptyList() {
        computeDoubleWrapper.setDoubleList(emptyList);

        assertThrows(OperationException.class, () -> computeDoubleWrapper.average());
    }

    @Test
    public void testFirstTenPercentEmptyList() {
        computeDoubleWrapper.setDoubleList(emptyList);
        List<Double> firstTenPercent = computeDoubleWrapper.firstTenPercent();

        assertThat(firstTenPercent.size(), is(equalTo(0)));
    }

    @Test
    public void testSumListWithNegativeValues() {
        computeDoubleWrapper.setDoubleList(listWithNegativeValues);
        Double sum = computeDoubleWrapper.sum();

        assertThat(sum, is(equalTo(10.0)));
    }

    @Test
    public void testAverageListWithNegativeValues() {
        computeDoubleWrapper.setDoubleList(listWithNegativeValues);
        Double average = computeDoubleWrapper.average();

        assertThat(average, is(equalTo(2.5)));
    }

    @Test
    public void testFirstTenPercentListWithNegativeValues() {
        List<Double> expectedResult = List.of(10.0);
        computeDoubleWrapper.setDoubleList(listWithNegativeValues);
        List<Double> firstTenPercent = computeDoubleWrapper.firstTenPercent();

        assertThat(firstTenPercent, is(equalTo(expectedResult)));
    }

}
