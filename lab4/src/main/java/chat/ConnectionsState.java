package chat;

import domain.EventType;
import domain.PeerInformation;

import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ConnectionsState {
    private final Connections activeConnections;
    private final Connections pendingConnections;
    private final Map<String, String> availableConnections;

    public ConnectionsState(Connections connections) {
        this.pendingConnections = connections;
        this.activeConnections = connections;
        this.availableConnections = new HashMap<>();
    }

    public synchronized void addPendingConnection(EventType eventType, String username, Socket socket) {
        boolean isNotActive = activeConnections.getConnection(username) == null;
        boolean isNotPending = pendingConnections.getConnection(username) == null;
        boolean isAvailable = availableConnections.get(username) != null;

        if (isNotPending && isNotActive) {
            if (EventType.INITIATE_CONNECTION == eventType && isAvailable) {
                pendingConnections.addConnection(username, socket, eventType);
            } else if (EventType.RECEIVE_CONNECTION == eventType) {
                pendingConnections.addConnection(username, socket, eventType);
            }
        }
    }

    public synchronized void updatePendingConnection(String username, Socket socket) {
        pendingConnections.updateConnection(username, socket);
    }

    public synchronized Socket getPendingConnection(String username) {
        return pendingConnections.getConnection(username);
    }

    public synchronized void removePendingConnection(String username) {
        pendingConnections.removeAndCloseConnection(username);
    }

    public synchronized void updateFromPendingToActive(String username) {
        Socket newActiveConnection = pendingConnections.removeConnection(username);
        activeConnections.addConnection(username, newActiveConnection, EventType.ACCEPT_CONNECTION);
    }

    public synchronized Socket getActiveConnection(String username) {
        return activeConnections.getConnection(username);
    }

    public synchronized void removeActiveConnection(String username) {
        activeConnections.removeAndCloseConnection(username);
    }

    public synchronized Set<String> getUsernamesOfActiveConnections() {
        return activeConnections.getConnections().keySet();
    }

    public synchronized void addAvailableConnection(PeerInformation peerInformation) {
        String ipAndPort = peerInformation.getIp() + ":" + peerInformation.getPort();
        availableConnections.put(peerInformation.getUsername(), ipAndPort);
    }

    public synchronized Set<String> getUsernamesOfAvailableConnections() {
        return availableConnections.keySet();
    }

    public synchronized String getIpAndPortForUser(String username) {
        return availableConnections.get(username);
    }
}
