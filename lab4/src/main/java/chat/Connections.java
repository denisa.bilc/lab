package chat;

import domain.EventType;
import observerPattern.ObservableConnections;
import observerPattern.Observer;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Connections implements ObservableConnections {
    private final Map<String, Socket> connections;
    private final List<Observer> observers;

    public Connections() {
        this.observers = new ArrayList<>();
        this.connections = new HashMap<>();
    }

    @Override
    public synchronized void addConnection(String username, Socket socket, EventType eventType) {
        connections.put(username, socket);
        notifyObservers(username, eventType);
    }

    @Override
    public synchronized Socket removeConnection(String username) {
        return connections.remove(username);
    }

    @Override
    public void removeAndCloseConnection(String username) {
        Socket socket = connections.remove(username);

        if (socket != null && !socket.isClosed()) {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public synchronized void updateConnection(String username, Socket socket) {
        connections.put(username, socket);
    }

    @Override
    public synchronized Socket getConnection(String username) {
        return connections.get(username);
    }

    @Override
    public Map<String, Socket> getConnections() {
        return connections;
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers(String username, EventType eventType) {
        for (Observer observer : observers) {
            observer.update(eventType, username);
        }
    }
}
