package chat;

import com.google.gson.Gson;
import domain.EventType;
import domain.CommandType;
import domain.Message;
import domain.PeerInformation;
import observerPattern.Observer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageReceiver implements Observer {

    private final ExecutorService executors;
    private final ConnectionsState connectionsState;
    private final PeerInformation peerInformation;

    public MessageReceiver(ConnectionsState connectionsState, PeerInformation peerInformation) {
        this.executors = Executors.newCachedThreadPool();
        this.connectionsState = connectionsState;
        this.peerInformation = peerInformation;
    }

    public void start() {
        executors.submit(this::receiveBroadcast);
    }

    private void showMessage(String username, String message) {
        System.out.println("[" + username + "]: " + message);
    }

    public void listenForMessagesFromActiveConnection(String username, Socket activeConnectionSocket) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(activeConnectionSocket.getInputStream()));
            Gson parser = new Gson();
            String receivedMessage;

            while ((receivedMessage = reader.readLine()) != null) {
                Message message = parser.fromJson(receivedMessage, Message.class);
                CommandType commandType = message.getMessageType();

                showMessage(username, message.getMessage());

                if (commandType == CommandType.BYE) {
                    break;
                }
            }

            connectionsState.removeActiveConnection(username);
        } catch (IOException e) {
            connectionsState.removeActiveConnection(username);
        }
    }

    public void listenForHelloBack(String username, Socket newConnectionSocket) {
        try {
            newConnectionSocket.setSoTimeout(120000);
            Gson parser = new Gson();
            BufferedReader reader = new BufferedReader(new InputStreamReader(newConnectionSocket.getInputStream()));
            String receivedMessage = reader.readLine();

            if (receivedMessage != null) {
                Message message = parser.fromJson(receivedMessage, Message.class);
                CommandType commandType = message.getMessageType();

                if (commandType == CommandType.HELLOBACK) {
                    showMessage(username, message.getMessage());
                    connectionsState.updateFromPendingToActive(username);
                } else {
                    connectionsState.removePendingConnection(username);
                }
            }
        } catch (IOException | IllegalArgumentException e) {
            connectionsState.removePendingConnection(username);
        }
    }

    public void receiveBroadcast() {
        try (DatagramSocket socket = new DatagramSocket(8888)) {
            socket.setBroadcast(true);
            Gson parser = new Gson();
            byte[] buffer = new byte[1024];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

            while (true) {
                Arrays.fill(buffer, (byte) 0);
                socket.receive(packet);
                String receivedData = new String(packet.getData(), 0, packet.getLength());
                PeerInformation receivedPeerInformation = parser.fromJson(receivedData, PeerInformation.class);

                if (!receivedPeerInformation.getUsername().equals(peerInformation.getUsername())) {
                    connectionsState.addAvailableConnection(receivedPeerInformation);
                }
            }
        } catch (IOException e) {
            System.out.println("[ERROR]: Can't receive broadcasts!");
        }
    }

    @Override
    public void update(EventType eventType, String username) {
        Socket socket;

        switch (eventType) {
            case INITIATE_CONNECTION:
                socket = connectionsState.getPendingConnection(username);
                if (socket != null) {
                    executors.submit(() -> listenForHelloBack(username, socket));
                }
                break;
            case RECEIVE_CONNECTION:
                showMessage(username, "HELLO");
                break;
            case ACCEPT_CONNECTION:
                socket = connectionsState.getActiveConnection(username);
                if (socket != null) {
                    executors.submit(() -> listenForMessagesFromActiveConnection(username, socket));
                }
                break;
        }
    }
}
