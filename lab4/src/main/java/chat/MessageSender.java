package chat;

import com.google.gson.Gson;
import domain.EventType;
import domain.CommandType;
import domain.Message;
import domain.PeerInformation;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.*;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageSender {
    private final PeerInformation currentPeerInformation;
    private final ExecutorService executors;
    private final ConnectionsState connectionsState;


    public MessageSender(PeerInformation peerInformation, ConnectionsState connectionsState) {
        this.currentPeerInformation = peerInformation;
        this.executors = Executors.newCachedThreadPool();
        this.connectionsState = connectionsState;
    }

    public void start() {
        executors.submit(this::getUserInput);
        executors.submit(this::sendBroadcast);
    }

    public void getUserInput() {
        Scanner scanner = new Scanner(System.in);
        String input;

        while (true) {
            try {
                input = scanner.nextLine();
                String[] parsedInput = input.split(" ");
                CommandType command = CommandType.valueOf(parsedInput[0].toUpperCase());

                switch (command) {
                    case HELLO:
                        executors.submit(() -> handleHelloCommand(parsedInput));
                        break;
                    case HELLOBACK:
                        executors.submit(() -> handleHelloBackCommand(parsedInput));
                        break;
                    case BYE:
                        executors.submit(() -> handleByeCommand(parsedInput));
                        break;
                    case BYEBYE:
                        executors.submit(() -> handleByeByeCommand());
                        break;
                    case PEERS:
                        executors.submit(() -> handlePeersCommand());
                        break;
                    default:
                        String finalInput = input;
                        executors.submit(() -> handleSendMessage(finalInput));
                        break;
                }
            } catch (IllegalArgumentException e) {
                System.out.println("[ERROR]: Invalid command!");
            }
        }
    }

    private void handleHelloCommand(String[] parsedInput) {
        if (parsedInput.length != 2) {
            return;
        }

        String username = parsedInput[1];
        connectionsState.addPendingConnection(EventType.INITIATE_CONNECTION, username, null);
    }

    private void handleHelloBackCommand(String[] parsedInput) {
        if (parsedInput.length != 2) {
            return;
        }

        String peerUsername = parsedInput[1];
        Socket socket = connectionsState.getPendingConnection(peerUsername);

        if (socket != null) {
            Message message = new Message(CommandType.HELLOBACK, "HELLOBACK");
            sendMessage(message, socket);

            connectionsState.updateFromPendingToActive(peerUsername);
        }
    }

    private void handleByeCommand(String[] parsedInput) {
        if (parsedInput.length != 2) {
            return;
        }

        String messageContent = parsedInput[0];
        String peerUsername = parsedInput[1];
        Socket peerSocket = connectionsState.getActiveConnection(peerUsername);
        Message message = new Message(CommandType.BYE, messageContent);

        sendMessage(message, peerSocket);
        connectionsState.removeActiveConnection(peerUsername);
    }

    private void handleByeByeCommand() {
        Set<String> usernamesOfActiveConnections = connectionsState.getUsernamesOfActiveConnections();

        for (String username : usernamesOfActiveConnections) {
            String[] input = {"BYE", username};
            executors.submit(() -> handleByeCommand(input));
        }
    }

    private void handleSendMessage(String input) {
        String[] parsedCommand = input.split(" ");
        if (parsedCommand.length < 2) {
            return;
        }

        String peerUsername = parsedCommand[1];
        int firstWhitespace = input.indexOf(" ");
        int secondWhitespace = input.indexOf(" ", firstWhitespace + 1);

        if (secondWhitespace == -1) {
            return;
        }

        String messageToSend = input.substring(secondWhitespace + 1);
        Socket socket = connectionsState.getActiveConnection(peerUsername);

        if (socket != null) {
            Message message = new Message(CommandType.MESSAGE, messageToSend);
            sendMessage(message, socket);
        }
    }

    private void handlePeersCommand() {
        Set<String> availablePeersUsernames = connectionsState.getUsernamesOfAvailableConnections();

        System.out.println("List of available peers: ");
        for (String peerUsername : availablePeersUsernames) {
            System.out.println(peerUsername);
        }
    }

    public void sendMessage(Message message, Socket socket) {
        try {
            Gson parser = new Gson();
            String messageJson = parser.toJson(message);
            OutputStream outputStream = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(outputStream, true);

            writer.println(messageJson);
        } catch (IOException e) {
            System.out.println("[ERROR]: Couldn't send message: " + message.getMessage());
        }
    }

    public void sendBroadcast() {
        try (DatagramSocket socket = new DatagramSocket()) {
            socket.setBroadcast(true);
            Gson parser = new Gson();
            String peerInformationJson = parser.toJson(currentPeerInformation);
            byte[] buffer = peerInformationJson.getBytes();
            InetAddress broadcastAddress = InetAddress.getByName("192.168.56.255");
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, broadcastAddress, 8888);

            while (true) {
                socket.send(packet);
                Thread.sleep(60000);
            }
        } catch (IOException | InterruptedException e) {
            System.out.println("[ERROR]: Can't send broadcasts!");
        }
    }
}
