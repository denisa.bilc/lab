package chat;

import domain.EventType;
import domain.PeerInformation;
import observerPattern.Observer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConnectionManager implements Observer {
    private final PeerInformation currentPeerInformation;
    private final ConnectionsState connectionsState;
    private final ExecutorService executors;

    public ConnectionManager(PeerInformation peerInformation, ConnectionsState connectionsState) {
        this.currentPeerInformation = peerInformation;
        this.connectionsState = connectionsState;
        this.executors = Executors.newCachedThreadPool();
    }

    public void start() {
        executors.submit(this::receiveConnections);
    }

    public void receiveConnections() {
        try (ServerSocket serverSocket = new ServerSocket(currentPeerInformation.getPort())) {
            while (true) {
                Socket clientSocket = serverSocket.accept();
                InputStream inputStream = clientSocket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String receivedUsername = reader.readLine();

                connectionsState.addPendingConnection(EventType.RECEIVE_CONNECTION, receivedUsername, clientSocket);
            }
        } catch (IOException e) {
            System.out.println("[ERROR]: Might not be able to receive connections!");
        }
    }

    public void initiateConnection(String peerUsername, String ip, int port) {
        String currentPeerUsername = currentPeerInformation.getUsername();

        try {
            Socket socket = new Socket(ip, port);
            connectionsState.updatePendingConnection(peerUsername, socket);

            OutputStream outputStream = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(outputStream, true);
            writer.println(currentPeerUsername);
        } catch (IOException e) {
            connectionsState.removePendingConnection(peerUsername);
        }
    }

    @Override
    public void update(EventType eventType, String peerUsername) {
        if (EventType.INITIATE_CONNECTION == eventType) {
            String[] ipAndPort = connectionsState.getIpAndPortForUser(peerUsername).split(":");
            String ip = ipAndPort[0];
            int port = Integer.parseInt(ipAndPort[1]);

            initiateConnection(peerUsername, ip, port);
        }
    }
}
