package chat;

import domain.PeerInformation;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.time.LocalDateTime;
import java.util.*;

public class Peer {
    private ConnectionManager connectionManager;
    private MessageReceiver messageReceiver;
    private MessageSender messageSender;
    private PeerInformation currentPeerInformation;

    public void startApp() {
        initializePeerInformation();
        initializeFields();
        callStartMethods();
    }

    private void initializePeerInformation() {
        getIpAndPort();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose a name: ");
        String currentPeerName = scanner.nextLine();

        String peerUsername = createUniqueUsername(currentPeerName);
        currentPeerInformation.setUsername(peerUsername);
        System.out.println("Your username is: " + peerUsername);

        showInstructions();
    }

    private String createUniqueUsername(String currentPeerName) {
        String peerIp = currentPeerInformation.getIp();
        String peerPort = String.valueOf(currentPeerInformation.getPort());
        String currentTime = LocalDateTime.now().toString();
        String dataToHash = currentPeerName + peerIp + peerPort + currentTime;

        UUID uuid = UUID.nameUUIDFromBytes(dataToHash.getBytes());
        String idToConcatenate = uuid.toString().substring(0, 8);

        return currentPeerName + idToConcatenate;
    }

    private void getIpAndPort() {
        try {
            String localhost = InetAddress.getLocalHost().getHostAddress();
            boolean foundPort = false;
            int port = 0;

            while (!foundPort) {
                ServerSocket socket = new ServerSocket(0);
                port = socket.getLocalPort();
                if (port != 8888) {
                    foundPort = true;
                }
                socket.close();
            }

            currentPeerInformation = new PeerInformation(localhost, port);
            System.out.println(localhost + ":" + port);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void showInstructions() {
        System.out.println("\n------------------------INSTRUCTIONS------------------------------\n");
        System.out.println("PEERS - shows available peers");
        System.out.println("HELLO <username> - initiates connection with the peer");
        System.out.println("HELLOBACK <username> - accepts connection with a peer");
        System.out.println("MESSAGE <username> <message> - sends message to the peer");
        System.out.println("BYE <username> - ends connection with the peer");
        System.out.println("BYEBYE - ends connection with all peers");
        System.out.println("\n------------------------INSTRUCTIONS------------------------------\n");
    }

    private void initializeFields() {
        Connections connections = new Connections();
        ConnectionsState connectionsState = new ConnectionsState(connections);
        this.connectionManager = new ConnectionManager(currentPeerInformation, connectionsState);
        this.messageSender = new MessageSender(currentPeerInformation, connectionsState);
        this.messageReceiver = new MessageReceiver(connectionsState, currentPeerInformation);

        connections.addObserver(this.connectionManager);
        connections.addObserver(this.messageReceiver);
    }

    private void callStartMethods() {
        this.connectionManager.start();
        this.messageReceiver.start();
        this.messageSender.start();
    }
}
