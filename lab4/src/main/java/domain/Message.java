package domain;

import java.util.Objects;

public class Message {
    private CommandType commandType;
    private String message;

    public Message(CommandType commandType, String message) {
        this.commandType = commandType;
        this.message = message;
    }

    public CommandType getMessageType() {
        return commandType;
    }

    public String getMessage() {
        return message;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;

        return commandType == message1.commandType && Objects.equals(message, message1.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commandType, message);
    }
}
