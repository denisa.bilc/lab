package domain;

public enum EventType {
    INITIATE_CONNECTION, RECEIVE_CONNECTION, ACCEPT_CONNECTION
}
