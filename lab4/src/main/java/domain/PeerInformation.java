package domain;

import java.util.Objects;

public class PeerInformation {
    private String username;
    private String ip;
    private int port;

    public PeerInformation(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeerInformation that = (PeerInformation) o;

        return port == that.port && Objects.equals(username, that.username) && Objects.equals(ip, that.ip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, ip, port);
    }
}
