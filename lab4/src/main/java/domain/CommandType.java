package domain;

public enum CommandType {
    HELLO, HELLOBACK, BYE, BYEBYE, MESSAGE, PEERS
}
