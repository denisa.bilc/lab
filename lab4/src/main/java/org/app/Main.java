package org.app;

import chat.Peer;

public class Main {
    public static void main(String[] args) {
        Peer peer = new Peer();
        peer.startApp();
    }
}