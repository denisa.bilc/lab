package observerPattern;

import domain.EventType;

import java.net.Socket;
import java.util.Map;

public interface ObservableConnections extends Observable {
    void addConnection(String username, Socket socket, EventType eventType);

    Socket removeConnection(String username);

    void removeAndCloseConnection(String username);

    void updateConnection(String username, Socket socket);

    Socket getConnection(String username);

    Map<String, Socket> getConnections();
}
