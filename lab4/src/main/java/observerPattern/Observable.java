package observerPattern;

import domain.EventType;

public interface Observable {
    void addObserver(Observer observer);

    void notifyObservers(String username, EventType eventType);
}
