package observerPattern;

import domain.EventType;

public interface Observer {
    void update(EventType eventType, String username);
}
